#encoding: utf-8

import re

class Stemmer:
    vowelRegion = u'(.*?[аеиоуыэюя])(.*)'
    perfectiveGerund = u'(((?P<infix>[ая])(в|вши|вшись))|(ив|ивши|ившись|ыв|ывши|ывшись))$'
    reflexive = u'(ся|сь)$'
    adjectival = u'(((?P<infix>[ая])(ем|нн|вш|ющ|щ))|(ивш|ывш|ующ))?'
    adjectival += u'(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|ему|ому|их|ых|ую|юю|ая|яя|ою|ею)$'
    verb = u'(((?P<infix>[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно))|'
    verb += u'(ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ен|ило|ыло|ено|ят|ует|уют|ит|ыт|ены|ить|ыть|ишь|ую|ю))$'
    noun = u'(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|иям|ям|ием|ем|ам|ом|о|у|ах|иях|ях|ы|ь|ию|ью|ю|ия|ья|я)$'
    derivational = u'(?P<infix>[^аеиоуыэюя].*?[аеиоуыэюя][^аеиоуыэюя].*?)(ост|ость)$'
    superlative = u'(ейш|ейше)$'
    
    def __init__(self, inputString):
        self.resultString = inputString
    
    def __str__(self):
        if not self.replace(Stemmer.perfectiveGerund):
            self.replace(Stemmer.reflexive)
            if not self.replace(Stemmer.adjectival):
                if not self.replace(Stemmer.verb):
                    self.replace(Stemmer.noun)
        self.replace(u'и$')
        self.replace(Stemmer.derivational)
        if self.replace(u'нн$', u'н'):
            pass
        elif self.replace(Stemmer.superlative):
            self.replace(u'нн$', u'н')
        else:
            self.replace(u'ь$')
        return self.resultString
    
    def replace(self, pattern, string = ''):
        match = re.match(Stemmer.vowelRegion, self.resultString)
        if match:
            prefix, postfix = match.groups()
            match = re.search(pattern, postfix)
            if match:
                if 'infix' in match.groupdict() and match.group('infix'): infix = match.group('infix')
                else: infix = ''
                postfix = re.sub(pattern, '{}{}'.format(infix, string), postfix)
                self.resultString = prefix + postfix
                return True
        return False