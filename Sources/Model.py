#encoding: utf-8

import os
import shutil
import warnings
import numpy
import scipy.cluster.vq
import matplotlib.pyplot
import Stemmer

class Word:
    def __init__(self, content):
        self.documents = []
        self.states = {'original': content}
        self.state = 'original'
    
    def __repr__(self):
        return self.states[self.state]
    
    def __eq__(self, word):
        return str(self) == str(word)
    
    def __lt__(self, word):
        return str(self) < str(word)
    
    def stem(self):
        self.states['stemmed'] = str(Stemmer.Stemmer(unicode(self)))
        self.state = 'stemmed'
    
    def __hash__(self):
        return hash(str(self))

class Document:
    def __init__(self, handle, title, content):
        self.handle = handle
        self.title = title
        self.content = content
    
    def __repr__(self):
        return self.content
    
    def makeWords(self, normalize = True):
        words = []
        filteredContent = filter(lambda symbol: symbol not in ',."—0123456789%', self.content)
        contentParts = filteredContent.split()
        if normalize:
            contentParts = [contentPart.lower() for contentPart in contentParts]
        for contentPart in contentParts:
            words.append(Word(contentPart))
        self.words = words
    
    def removeStopSymbols(self, stopSymbols):
        self.words = filter(lambda word: word not in stopSymbols, self.words)
    
    def stemWords(self):
        for word in self.words: word.stem()
    
    def countWord(self, word):
        wordCount = 0
        for documentWord in self.words:
            if documentWord == word:  wordCount += 1
        return wordCount
    
class Corpus(list):
    def __init__(self):
        self.nextDocumentHandle = 1
    
    def createDocument(self, title, content):
        self.append(Document(self.nextDocumentHandle, title, content))
        self.nextDocumentHandle += 1
    
    def makeWords(self):
        print 'Making words...'
        corpusWords = {}
        for document in self:
            for documentWord in document.words:
                documentWordContent = str(documentWord)
                if documentWordContent not in corpusWords: corpusWords[documentWordContent] = documentWord
                corpusWords[documentWordContent].documents.append(document)
        self.words = list(corpusWords.values())
    
    def makeDocumentWords(self, normalize = True):
        for document in self: document.makeWords(normalize)
    
    def removeStopSymbols(self, fileName):
        print 'Removing stop symbols...'
        stopSymbols = set([Word(content) for content in open(fileName).read().split()])
        for document in self: document.removeStopSymbols(stopSymbols)
    
    def stemDocumentWords(self):
        print 'Stemming words...'
        for document in self: document.stemWords()
    
    def removeRareWords(self, rarity):
        print 'Removing single words...'
        for word in self.words[:]:
            if len(word.documents) <= rarity: self.words.remove(word)
    
    def makeWordMatrix(self):
        wordMatrix = []
        for word in self.words:
            wordMatrixRow = []
            inverseDocumentFrequency = float(len(self)) / len(word.documents)
            for document in self:
                termFrequency = float(document.countWord(word)) / len(document.words)
                wordMatrixRow.append(termFrequency * inverseDocumentFrequency)
            wordMatrix.append(wordMatrixRow)
        self.wordMatrix = wordMatrix
    
    def sortWords(self):
        print 'Sorting words...'
        self.words = sorted(self.words)
    
    def computeSVD(self):
        print 'Computing SVD...'
        wordVertices, singularValues, documentVertices = numpy.linalg.svd(self.wordMatrix)
        dimensionCount = 5
        wordVertices = wordVertices[:len(wordVertices), :dimensionCount]
        singularValues = numpy.diag(singularValues)[:dimensionCount, :dimensionCount]
        documentVertices = documentVertices[:dimensionCount, :len(documentVertices)].transpose()
        self.wordVertices = wordVertices
        self.documentVertices = documentVertices
    
    def clusterizeDocuments(self, folderName):
        print 'Clusterizing documents...'
        warnings.simplefilter('ignore')
        result = scipy.cluster.vq.kmeans2(self.documentVertices, self.wordVertices, minit = 'matrix')
        self.documentCentroids, self.documentClusters = result
        clusterNames = {}
        for documentCluster in self.documentClusters:
            clusterName = self.words[documentCluster].states['original']
            if clusterName not in clusterNames: clusterNames[clusterName] = None
        print 'Cluster list:', ', '.join(clusterNames)
        shutil.rmtree(folderName)
        os.mkdir(folderName)
        for document, documentCluster in zip(self, self.documentClusters):
            clusterName = self.words[documentCluster].states['original']
            documentTitle = filter(lambda symbol: symbol not in '":', document.title)
            clusterFolderName = os.path.join(folderName, clusterName)
            if not os.path.isdir(clusterFolderName.encode('cp1251')):
                os.mkdir(clusterFolderName.encode('cp1251'))
            fileName = os.path.join(clusterFolderName, documentTitle)
            open(fileName, 'w').write(document.content)
    
    def visualize(self, fileName):
        print 'Visualizing results...'
        axes = matplotlib.pyplot.figure().add_subplot(1, 1, 1)
        axes.scatter(*self.wordVertices.transpose()[:2, :len(self.wordVertices)], color = (1, 0, 0))
        axes.scatter(*self.documentVertices.transpose()[:2, :len(self.documentVertices)], color = (0, 0, 0))
        axes.scatter(*self.documentCentroids.transpose()[:2, :len(self.documentCentroids)], color = (0, 0, 1))
        axes.figure.savefig(fileName)