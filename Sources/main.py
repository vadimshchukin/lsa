#!/usr/bin/env python
#encoding: utf-8

import Parser
import Model

def main(updateDocuments, feedLocation, documentsFileName):
    parser = Parser.Parser()
    parser.loadDocuments(documentsFileName)
    if updateDocuments:
        parser.updateDocuments(feedLocation)
        parser.saveDocuments(documentsFileName)
    corpus = Model.Corpus()
    parser.loadIntoCorpus(corpus)
    corpus.makeDocumentWords()
    corpus.removeStopSymbols('../Data/stopSymbols.txt')
    corpus.stemDocumentWords()
    corpus.makeWords()
    corpus.removeRareWords(25)
    corpus.sortWords()
    corpus.makeWordMatrix()
    corpus.computeSVD()
    corpus.clusterizeDocuments('../Output')
    corpus.visualize('../Data/clusterization.png')

if __name__ == '__main__': main(False, 'http://ria.ru/export/rss2/economy/index.xml', '../Input')