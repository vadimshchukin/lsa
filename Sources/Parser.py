#encoding: utf-8

import os
import re
import feedparser
import pyquery

class Parser:
    def __init__(self):
        self.documents = []
    
    def parsePage(self, pageLocation):
        selector = pyquery.PyQuery(url = pageLocation)
        documentContent = selector('.body').text()
        documentContent = re.sub(u'©.+?\| Купить иллюстрацию .+?\. ', '', documentContent)
        documentContent = re.sub(u'^.*?— (РИА Новости|Прайм).*?\. ', '', documentContent)
        return documentContent
    
    def parseFeed(self, feedLocation):
        feed = feedparser.parse(feedLocation)
        documents = []
        for entryIndex in range(len(feed['entries'])):
            print 'Parsing document {} of {}...'.format(entryIndex + 1, len(feed['entries']))
            entry = feed['entries'][entryIndex]
            entryLink = entry['link'].replace(u'www.ria.ru', u'm.ria.ru')
            documents.append((entry['title'], self.parsePage(entryLink)))
        return documents
    
    def saveDocuments(self, folderName):
        for document in self.documents:
            title, content = document
            title = filter(lambda symbol: symbol not in ':"', title)
            open(os.path.join(folderName, title), 'w').write(content)
    
    def loadDocuments(self, folderName):
        print 'Loading documents...'
        for rootName, folderNames, fileNames in os.walk(folderName):
            for fileName in fileNames:
                fullFileName = os.path.join(rootName, fileName)
                fileContent = open(fullFileName).read()
                self.documents.append((unicode(fileName.decode('cp1251')), unicode(fileContent)))
    
    def loadIntoCorpus(self, corpus):
        for document in self.documents: corpus.createDocument(*document)
    
    def updateDocuments(self, feedLocation):
        newDocuments = self.parseFeed(feedLocation)
        for newDocument in newDocuments:
            newDocumentTitle, newDocumentContent = newDocument
            documentFound = False
            for oldDocument in self.documents:
                oldDocumentTitle, oldDocumentContent = oldDocument
                if oldDocumentTitle == newDocumentTitle:
                    documentFound = True
                    break
            if not documentFound: self.documents.append(newDocument)